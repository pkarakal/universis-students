import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { RequestsHomeComponent } from './requests-home.component';
// important: exclude test because of a karma error on linux
// todo: fix error on linux
describe('RequestsHomeComponent', () => {
  let component: RequestsHomeComponent;
  let fixture: ComponentFixture<RequestsHomeComponent>;

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ RequestsHomeComponent ],
      imports: [RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
